use crate::{ Result, Error, Time };

#[derive(Debug)]
pub struct MemInfo {
    pub tm:		Time,
    pub mem_total:	u64,
    pub mem_free:	u64,
    pub mem_avail:	u64,
    pub buffers:	u64,
    pub cached:		u64,
    pub swap:		u64,
}

impl MemInfo {
    fn parse_unit(unit: &str) -> Result<u64> {
	match unit {
	    "kB"	=> Ok(1_024),
	    u		=> {
		error!("unknown unit {u}");
		Err(Error::BadMemInfo("bad unit"))
	    }
	}
    }
    fn parse_value(v: &str) -> Result<u64> {
	let v = v.trim_start();

	let (v, mul) = match v.split_once(' ') {
	    Some((v, unit))	=> (v, Self::parse_unit(unit)?),
	    None		=> (v, 1),
	};

	Ok(mul * v.parse::<u64>()
	   .map_err(|_| Error::BadMemInfo("can not convert value to int"))?)
    }

    pub fn read(now: Time) -> Result<Self> {
	let content  = std::fs::read_to_string("/proc/meminfo")?;

	let mut v_mem_total = None;
	let mut v_mem_free = None;
	let mut v_mem_avail = None;
	let mut v_buffers = None;
	let mut v_cached = None;
	let mut v_swap = None;

	for l in content.split('\n') {
	    let (key, val) = match l.split_once(':') {
		Some(v)	=> v,
		None	=> continue,
	    };
	    let val = Self::parse_value(val)?;

	    match key {
		"MemTotal"	=> v_mem_total = Some(val),
		"MemFree"	=> v_mem_free = Some(val),
		"MemAvailable"	=> v_mem_avail = Some(val),
		"Buffers"	=> v_buffers = Some(val),
		"Cached"	=> v_cached = Some(val),
		"SwapCached"	=> v_swap = Some(val),
		_		=> {},
	    }
	}

	Ok(Self {
	    tm:	now,
	    mem_total:	v_mem_total.ok_or(Error::BadMemInfo("missing 'MemTotal'"))?,
	    mem_free:	v_mem_free.ok_or(Error::BadMemInfo("missing 'MemFree'"))?,
	    mem_avail:	v_mem_avail.ok_or(Error::BadMemInfo("missing 'MemAvailable'"))?,
	    buffers:	v_buffers.ok_or(Error::BadMemInfo("missing 'Buffers'"))?,
	    cached:	v_cached.ok_or(Error::BadMemInfo("missing 'Cached'"))?,
	    swap:	v_swap.ok_or(Error::BadMemInfo("missing 'Swap'"))?,
	})
    }
}
