#[derive(Clone, Debug)]
pub struct IfaceMatch {
    pub is_neg:		bool,
    pub regexp:		regex::Regex,
}

impl clap::builder::ValueParserFactory for IfaceMatch {
    type Parser = IfaceMatchParser;

    fn value_parser() -> Self::Parser {
        IfaceMatchParser
    }
}

impl IfaceMatch {
    pub fn matches(&self, name: &str) -> IfaceMatchResult {
	match (self.regexp.is_match(name), self.is_neg) {
	    (true, true)	=> IfaceMatchResult::Neg,
	    (true, false)	=> IfaceMatchResult::Yes,
	    (false, true)	=> IfaceMatchResult::Yes,
	    (false, false)	=> IfaceMatchResult::No,
	}
    }

    pub fn matches_spec(spec: &Vec<Self>, name: &str) -> IfaceMatchResult {
	let mut res = IfaceMatchResult::No;

	for iface in spec {
	    res.update(|| iface.matches(name));
	}

	res
    }
}

#[derive(Debug, Clone, Copy)]
pub enum IfaceMatchResult {
    Yes,
    No,
    Neg,
}

impl IfaceMatchResult {
    pub fn update<T: FnOnce() -> Self>(&mut self, f: T) {
	*self = match self {
	    Self::Yes | Self::Neg	=> *self,
	    Self::No			=> f(),
	}
    }

    pub fn is_yes(&self) -> bool {
	matches!(self, Self::Yes)
    }
}

#[derive(Clone, Debug)]
pub struct IfaceMatchParser;

impl clap::builder::TypedValueParser for IfaceMatchParser {
    type Value = IfaceMatch;

    fn parse_ref(&self, cmd: &clap::Command, _arg: Option<&clap::Arg>,
		 value: &std::ffi::OsStr) -> Result<Self::Value, clap::Error> {
	let value = value.to_str()
	    .ok_or_else(||
			clap::Error::raw(clap::error::ErrorKind::InvalidValue, "utf-8 conversion error")
			.with_cmd(cmd))?;

	if value.is_empty() {
	    return Err(clap::Error::raw(clap::error::ErrorKind::InvalidValue, "interface must not be empty")
		       .with_cmd(cmd));
	}

	let (is_neg, regexp) = match value.as_bytes()[0] {
	    b'!'	=> (true, &value[1..]),
	    _		=> (false, value),
	};

	Ok(IfaceMatch {
	    is_neg:	is_neg,
	    regexp:	regex::Regex::new(regexp)
		.map_err(|e|
			 clap::Error::raw(clap::error::ErrorKind::InvalidValue, e).with_cmd(cmd))?,
	})
    }
}
