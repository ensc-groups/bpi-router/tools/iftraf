use nix::libc;
use crate::{ Error, Result };

pub const NLMSG_ALIGNTO: usize = 4;

#[repr(transparent)]
#[derive(Clone, Copy, Debug)]
pub struct nlmsghdr(libc::nlmsghdr);

impl std::ops::Deref for nlmsghdr {
    type Target = libc::nlmsghdr;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::ops::DerefMut for nlmsghdr {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl From<libc::nlmsghdr> for nlmsghdr {
    fn from(value: libc::nlmsghdr) -> Self {
        Self(value)
    }
}

impl From<nlmsghdr> for libc::nlmsghdr {
    fn from(value: nlmsghdr) -> Self {
        value.0
    }
}

impl nlmsghdr {
    #[inline]
    pub const fn align_up(len: usize) -> usize {
	(len + NLMSG_ALIGNTO - 1) / NLMSG_ALIGNTO * NLMSG_ALIGNTO
    }

    #[inline(always)]
    pub const fn hdr_size() -> usize {
	Self::align_up(core::mem::size_of::<Self>())
    }

    #[inline]
    pub const fn total_len(&self) -> usize {
	self.0.nlmsg_len as usize
    }

    #[inline]
    pub const fn total_len_aligned(&self) -> usize {
	Self::align_up(self.total_len())
    }

    #[instrument(level=tracing::Level::TRACE, ret)]
    fn split(buf: &[u8]) -> Result<&Self> {
	let (prefix, data, _) = unsafe { buf.align_to::<Self>() };

	if !prefix.is_empty() {
	    debug!("prefix={:x?}, data={:?}", prefix, data.first());
	    return Err(Error::Misaligned("nlmsghdr"));
	}

	if data.is_empty() {
	    debug!("buf={:x?}", buf);
	    return Err(Error::InsufficientBuffer("nlmsghdr", buf.len()));
	}

	let sz = data[0].total_len();

	if sz < core::mem::size_of::<Self>() {
	    debug!("hdr={:x?}", data[0]);
	    return Err(Error::TooSmall("nlmsghdr", sz));
	}

	if sz > buf.len() {
	    debug!("hdr={:x?}, #buf={}", data[0], buf.len());
	    return Err(Error::TooLarge("nlmsghdr", sz));
	}

	Ok(&data[0])
    }

    pub fn next<'a>(&self, buf: &'a [u8]) -> Option<&'a [u8]> {
	let sz  = self.total_len_aligned();

	if sz >= buf.len() {
	    None
	} else {
	    Some(&buf[sz..])
	}
    }

    #[instrument(level=tracing::Level::TRACE, skip(buf), ret)]
    pub fn payload<'a>(&self, buf: &'a [u8]) -> &'a [u8] {
	let pos = Self::hdr_size();

	if pos <= buf.len() {
	    &buf[pos..self.total_len()]
	} else {
	    &[]
	}
    }

    #[inline]
    pub fn from_slice(buf: &[u8]) -> Result<&Self> {
	Self::split(buf)
    }

    #[inline]
    pub fn from_slice_unchecked(buf: &[u8]) -> &Self {
	&unsafe { buf.align_to::<Self>() }.1[0]
    }
}
