use nix::libc;
use crate::{ Result, Error };

#[repr(C)]
#[derive(Debug, Clone)]
pub struct ifinfomsg {
    pub ifi_family:		libc::c_uchar,
    pub ifi_type:		libc::c_ushort,
    pub ifi_index:		libc::c_int,
    pub ifi_flags:		libc::c_uint,
    pub ifi_change:		libc::c_uint,
}

impl ifinfomsg {
    #[inline]
    pub fn as_bytes(&self) -> &[u8] {
	unsafe { core::slice::from_raw_parts(self as * const _ as * const u8,
					     core::mem::size_of_val(self)) }
    }

    #[inline]
    pub const fn align_up(len: usize) -> usize {
	super::c_rtattr::align_up(len)
    }

    #[inline(always)]
    pub const fn hdr_size() -> usize {
	Self::align_up(core::mem::size_of::<Self>())
    }

    pub fn from_slice(buf: &[u8]) -> Result<&Self> {
	let (prefix, data, _) = unsafe { buf.align_to::<Self>() };

	if !prefix.is_empty() {
	    debug!("prefix={:x?}, data={:?}", prefix, data.first());
	    return Err(Error::Misaligned("ifinfomsg"));
	}

	if data.is_empty() {
	    debug!("buf={:x?}", buf);
	    return Err(Error::InsufficientBuffer("ifinfomsg", buf.len()));
	}

	Ok(&data[0])
    }

    pub fn payload<'a>(&self, buf: &'a [u8]) -> &'a [u8] {
	let pos = Self::hdr_size();

	if pos <= buf.len() {
	    &buf[pos..]
	} else {
	    &[]
	}
    }
}

#[repr(C)]
#[derive(Debug)]
pub struct rtnl_link_stats {
    pub rx_packets:		u32,
    pub tx_packets:		u32,
    pub rx_bytes:		u32,
    pub tx_bytes:		u32,
    pub rx_errors:		u32,
    pub tx_errors:		u32,
    pub rx_dropped:		u32,
    pub tx_dropped:		u32,
    pub multicast:		u32,
    pub collisions:		u32,

    /* detailed rx_errors: */
    pub rx_length_errors:	u32,
    pub rx_over_errors:		u32,
    pub rx_crc_errors:		u32,
    pub rx_frame_errors:	u32,
    pub rx_fifo_errors:		u32,
    pub rx_missed_errors:	u32,

    /* detailed tx_errors */
    pub tx_aborted_errors:	u32,
    pub tx_carrier_errors:	u32,
    pub tx_fifo_errors:		u32,
    pub tx_heartbeat_errors:	u32,
    pub tx_window_errors:	u32,

    /* for cslip etc */
    pub rx_compressed:		u32,
    pub tx_compressed:		u32,

    pub rx_nohandler:		u32,
}

#[repr(C, packed(4))]
#[derive(Debug, Copy, Clone)]
pub struct rtnl_link_stats64 {
    pub rx_packets:		u64,
    pub tx_packets:		u64,
    pub rx_bytes:		u64,
    pub tx_bytes:		u64,
    pub rx_errors:		u64,
    pub tx_errors:		u64,
    pub rx_dropped:		u64,
    pub tx_dropped:		u64,
    pub multicast:		u64,
    pub collisions:		u64,

    /* detailed rx_errors: */
    pub rx_length_errors:	u64,
    pub rx_over_errors:		u64,
    pub rx_crc_errors:		u64,
    pub rx_frame_errors:	u64,
    pub rx_fifo_errors:		u64,
    pub rx_missed_errors:	u64,

    /* detailed tx_errors */
    pub tx_aborted_errors:	u64,
    pub tx_carrier_errors:	u64,
    pub tx_fifo_errors:		u64,
    pub tx_heartbeat_errors:	u64,
    pub tx_window_errors:	u64,

    /* for cslip etc */
    pub rx_compressed:		u64,
    pub tx_compressed:		u64,
    pub rx_nohandler:		u64,

    // only for kernel >5.15
//    pub rx_otherhost_dropped:	u64,
}
