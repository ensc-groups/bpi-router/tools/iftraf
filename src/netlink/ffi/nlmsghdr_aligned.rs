use nix::libc;

const PAD_SIZE: usize = {
    use super::nlmsghdr;
    let sz = core::mem::size_of::<nlmsghdr>();
    nlmsghdr::align_up(sz) - sz
};

#[repr(C)]
pub struct nlmsghdr_aligned {
    hdr:	super::nlmsghdr,
    _pad:	[u8; PAD_SIZE],
}

impl nlmsghdr_aligned {
    pub fn with_seq(orig: libc::nlmsghdr, seq: u32, len: usize) -> Self {
	let orig = libc::nlmsghdr {
	    nlmsg_seq:	seq,
	    // TOOD: align 'len'?
	    nlmsg_len:	(len + core::mem::size_of::<Self>()) as u32,
	    .. orig
	};

	Self {
	    hdr:	orig.into(),
	    _pad:	unsafe { core::mem::zeroed() }
	}
    }

    pub fn as_bytes(&self) -> &[u8] {
	unsafe { std::slice::from_raw_parts(self as * const _ as * const u8,
					    core::mem::size_of::<Self>()) }
    }
}
