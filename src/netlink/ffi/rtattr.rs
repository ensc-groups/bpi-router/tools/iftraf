use nix::libc;

use crate::{ Error, Result };

pub const RTA_ALIGNTO: usize = 4;

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct c_rtattr {
    pub rta_len:	libc::c_ushort,
    pub rta_type:	libc::c_ushort,
}

impl c_rtattr {
    #[inline]
    pub const fn align_up(len: usize) -> usize {
	(len + RTA_ALIGNTO - 1) / RTA_ALIGNTO * RTA_ALIGNTO
    }

    #[inline(always)]
    pub const fn hdr_size() -> usize {
	Self::align_up(core::mem::size_of::<Self>())
    }

    #[inline]
    pub const fn total_len(&self) -> usize {
	self.rta_len as usize
    }

    #[inline]
    pub const fn total_len_aligned(&self) -> usize {
	Self::align_up(self.total_len())
    }

    #[instrument(level=tracing::Level::TRACE, ret)]
    fn split(buf: &[u8]) -> Result<&Self> {
	let (prefix, data, _) = unsafe { buf.align_to::<Self>() };

	if !prefix.is_empty() {
	    return Err(Error::Misaligned("rtattr"));
	}

	if data.is_empty() {
	    return Err(Error::InsufficientBuffer("rtattr", buf.len()));
	}

	let sz = data[0].total_len();

	if sz < Self::hdr_size() {
	    return Err(Error::TooSmall("rtattr", sz));
	}

	if sz > buf.len() {
	    return Err(Error::TooLarge("rtattr", sz));
	}

	Ok(&data[0])
    }

    pub fn next<'a>(&self, buf: &'a [u8]) -> Option<&'a [u8]> {
	let sz  = self.total_len_aligned();

	if sz >= buf.len() {
	    None
	} else {
	    Some(&buf[sz..])
	}
    }

    #[inline]
    pub fn payload<'a>(&self, buf: &'a [u8]) -> &'a [u8] {
	let pos = Self::hdr_size();

	&buf[pos..(self.total_len())]
    }

    #[inline]
    pub fn from_slice(buf: &[u8]) -> Result<&Self> {
	Self::split(buf)
    }

    #[inline]
    pub fn from_slice_unchecked(buf: &[u8]) -> &Self {
	&unsafe { buf.align_to::<Self>() }.1[0]
    }
}
