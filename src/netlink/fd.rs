use std::os::unix::io::{ AsRawFd, RawFd };
use nix::{ libc, sys::socket };

use crate::{ Result, Error };
use crate::netlink::nlmsg::NlMsgStream;

pub struct Fd {
    fd:			crate::Fd,
    seq:		std::sync::atomic::AtomicU32,
}

impl AsRawFd for Fd {
    fn as_raw_fd(&self) -> RawFd {
        self.fd.as_raw_fd()
    }
}

impl Fd {
    pub fn new() -> Result<Self> {
	let fd = unsafe {
	    libc::socket(libc::AF_NETLINK,
			 libc::SOCK_RAW | libc::SOCK_CLOEXEC,
			 libc::NETLINK_ROUTE)
	};

	Error::try_rc(fd)?;

	let fd = crate::Fd::new(fd);

	let mut addr: libc::sockaddr_nl = unsafe { core::mem::zeroed() };

	addr.nl_family = libc::AF_NETLINK as u16;
	addr.nl_groups = libc::RTNLGRP_LINK;

	let rc = unsafe {
	    libc::bind(fd.as_raw_fd(), &addr as * const _ as * const libc::sockaddr,
		       core::mem::size_of_val(&addr) as libc::socklen_t)
	};

	Error::try_rc(rc)?;

	Ok(Self {
	    fd:			fd,
	    seq:		std::sync::atomic::AtomicU32::new(0),
	})
    }

    fn wait_for_rx(&self, duration: std::time::Duration) -> Result<()> {
	let mut tm = nix::sys::time::TimeVal::new(duration.as_secs() as libc::time_t,
					      (duration.as_micros() % 1_000_000) as libc::suseconds_t);
	let mut fds = nix::sys::select::FdSet::new();

	fds.insert(self.fd.as_raw_fd());

	match nix::sys::select::select(None, Some(&mut fds), None, None, Some(&mut tm)) {
	    Err(e)	=> Err(e)?,
	    Ok(0)	=> Err(nix::Error::ETIMEDOUT)?,
	    Ok(_)	=> Ok(())
	}
    }

    fn get_flags(is_blocking: bool) -> socket::MsgFlags {
	use socket::MsgFlags;

	match is_blocking {
	    true	=> MsgFlags::empty(),
	    false	=> MsgFlags::MSG_DONTWAIT,
	}
    }

    /// Returns an unique sequence number.
    ///
    /// The value '0' is not used so that responses can be distinguished from
    /// asynchronous netlink messages (e.g. generated when interfaces are
    /// added or removed).
    pub fn next_seq(&self) -> u32 {
	loop {
	    match self.seq.fetch_add(1, std::sync::atomic::Ordering::Relaxed) {
		0	=> continue,
		v	=> break v,
	    }
	}
    }

    pub fn send_request(&self, hdr: libc::nlmsghdr, msg: &[u8],
			is_blocking: bool) -> Result<Request> {
	use std::io::IoSlice;

	let seq = self.next_seq();
	let hdr = super::ffi::nlmsghdr_aligned::with_seq(hdr, seq, msg.len());

	let req = [
	    IoSlice::new(hdr.as_bytes()),
	    IoSlice::new(msg)
	];
	let total_sz = req.iter().fold(0, |acc, e| acc + e.len());

	let sz = socket::sendmsg(self.fd.as_raw_fd(), &req, &[],
				 Self::get_flags(is_blocking),
				 Option::<&socket::NetlinkAddr>::None)?;

	assert_eq!(sz, total_sz);

	Ok(Request {
	    seq:	seq
	})
    }

    fn remaining(start: Option<std::time::Instant>, timeout: Option<std::time::Duration>) -> std::time::Duration {
	match (start, timeout) {
	    (Some(s), Some(t))	=> {
		let duration = s.elapsed();

		if duration > t {
		    std::time::Duration::ZERO
		} else {
		    t - duration
		}
	    },
	    _			=> std::time::Duration::ZERO,
	}
    }

    pub fn recv<'a>(&self, buf: &'a mut [core::mem::MaybeUninit<u8>],
		    timeout: Option<std::time::Duration>) -> Result<NlMsgStream<'a>> {
	let resp = self.recv_raw(buf, timeout)?;

	NlMsgStream::new(resp)
    }

    fn recv_raw<'a>(&self, buf: &'a mut [core::mem::MaybeUninit<u8>],
		    timeout: Option<std::time::Duration>) -> Result<&'a [u8]> {
	let a_slice: &'a mut [u8] = unsafe { core::mem::transmute(buf) };
	let flags = Self::get_flags(timeout.is_none());
	let start = timeout.as_ref().map(|_| std::time::Instant::now());

	let sz = loop {
	    match socket::recv(self.fd.as_raw_fd(), a_slice, flags) {
		Ok(sz)			=> break Ok(sz),
		Err(nix::Error::EAGAIN) => {
		    let remaining = Self::remaining(start, timeout);
		    if remaining.is_zero() {
			break Err(nix::Error::ETIMEDOUT);
		    }

		    self.wait_for_rx(remaining)?;
		}
		Err(e)			=> break Err(e),
	    }
	}?;

	Ok(&a_slice[0..sz])
    }

//    pub async fn send_request_async(&self, req: &[u8]) -> Result<()> {
//    }
}

pub struct Request {
    pub seq:	u32
}
