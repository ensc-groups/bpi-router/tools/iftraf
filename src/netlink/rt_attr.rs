use nix::libc;
use crate::Result;

use super::ffi::c_rtattr;
use super::rtattr;

#[derive(Clone, Debug)]
pub struct RtAttrStream<'a> {
    buf:	&'a [u8],
    attr:	c_rtattr,
}

impl <'a> RtAttrStream<'a> {
    fn verify(buf: &'a [u8]) -> Result<()> {
	let mut buf = if buf.is_empty() {
	    None
	} else {
	    Some(buf)
	};

	while let Some(b) = buf {
	    let attr = c_rtattr::from_slice(b)?;
	    buf = attr.next(b);
	}

	Ok(())
    }

    pub fn try_into_ifla(self) -> Result<rtattr::IFLA<'a>> {
	rtattr::IFLA::from_slice(self.get_type(), self.get_payload())
    }

    pub fn get_type(&self) -> libc::c_ushort {
	self.attr.rta_type
    }

    pub fn get_payload(&self) -> &'a [u8] {
	self.attr.payload(self.buf)
    }

    pub fn new(buf: &'a [u8]) -> Result<Self> {
	Self::verify(buf)?;

	Ok(Self::new_unchecked(buf))
    }

    #[tracing::instrument(level=tracing::Level::TRACE, skip(buf), ret)]
    fn new_unchecked(buf: &'a [u8]) -> Self {
	Self {
	    buf:	buf,
	    attr:	*c_rtattr::from_slice_unchecked(buf),
	}
    }

    pub fn next(&self) -> Option<Self> {
	self.attr.next(self.buf).map(Self::new_unchecked)
    }
}

impl <'a> IntoIterator for RtAttrStream<'a> {
    type Item = RtAttrStream<'a>;
    type IntoIter = RtAttrIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
	RtAttrIterator {
	    attr:	Some(self)
	}
    }
}

pub struct RtAttrIterator<'a> {
    attr:	Option<RtAttrStream<'a>>,
}

impl <'a> Iterator for RtAttrIterator<'a> {
    type Item = RtAttrStream<'a>;

    fn next(&mut self) -> Option<Self::Item> {
	let attr = self.attr.take();

	match attr {
	    None	=> None,
	    Some(a)	=> {
		self.attr = a.next();
		Some(a)
	    }
	}
    }
}
