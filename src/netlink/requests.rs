use nix::libc;
use super::ffi::ifinfomsg;

pub static RTM_GETLINK: (libc::nlmsghdr, ifinfomsg) = (
    libc::nlmsghdr {
	nlmsg_type:	libc::RTM_GETLINK,
	nlmsg_flags:	(libc::NLM_F_REQUEST | libc::NLM_F_DUMP) as u16,
        nlmsg_len:	0,
        nlmsg_seq:	0,
        nlmsg_pid:	0,
    },

    ifinfomsg {
	ifi_family:	libc::AF_UNSPEC as u8,
        ifi_type:	0,
        ifi_index:	0,
        ifi_flags:	libc::IFF_UP as u32,
        ifi_change:	0,
    }
);
