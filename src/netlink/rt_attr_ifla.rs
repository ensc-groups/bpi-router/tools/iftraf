use nix::libc;

use crate::{ Error, Result };
use super::ffi;

#[derive(Debug)]
pub enum RtAttrIFLA<'a> {
    Address(&'a[u8]),
    IfName(&'a std::ffi::CStr),
    Mtu(libc::c_uint),
    Link(libc::c_int),
    ProtInfo(&'a [u8]),
    Stats(&'a ffi::rtnl_link_stats),
    Stats64(&'a ffi::rtnl_link_stats64),
    Raw(libc::c_ushort, &'a [u8]),
    TxQLen(libc::c_uint),
    OperState(libc::c_uchar),
    LinkMode(libc::c_uchar),
}

impl <'a> RtAttrIFLA<'a> {
    fn cast_buf<T: Sized>(buf: &'a [u8], hint: &'static str) -> Result<&'a T> {
	let (prefix, data, _) = unsafe { buf.align_to::<T>() };

	if !prefix.is_empty() {
	    warn!("{} is unaligned #buf={} #T={} ({:x?}/{}/{})", hint, buf.len(),
		  core::mem::size_of::<T>(), prefix, prefix.len(), data.len());
	    return Err(Error::NlResponseMisaligned);
	}

	match data.len() {
	    0	=> {
		warn!("{:x?} too small for {}; need {}", buf, hint, core::mem::size_of::<T>());
		Err(Error::NlResponseBadSize(buf.len()))
	    },

	    1	=> Ok(&data[0]),

	    _	=> {
		warn!("too much data ({}) for {}", buf.len(), hint);
		Err(Error::NlResponseBadSize(buf.len()))
	    }
	}
    }

    pub fn from_slice(rta_type: libc::c_ushort, buf: &'a [u8]) -> Result<Self> {
	Ok(match rta_type {
	    libc::IFLA_ADDRESS	=> Self::Address(buf),
	    libc::IFLA_MTU	=> Self::Mtu(*Self::cast_buf(buf, "IFLA_MTU")?),
	    libc::IFLA_IFNAME	=> Self::IfName(std::ffi::CStr::from_bytes_with_nul(buf)
						.map_err(|_| Error::BadCString("ifname"))?),
	    libc::IFLA_LINK	=> Self::Link(*Self::cast_buf(buf, "IFLA_LINK")?),
	    libc::IFLA_STATS	=> Self::Stats(Self::cast_buf(buf, "IFLA_STATS")?),
	    // TODO: rtnl_link_stats64 differs between kernel versions; we use
	    // the minimal version but should solve this in a more clean way
	    libc::IFLA_STATS64	=> Self::Stats64(Self::cast_buf(buf, "IFLA_STATS64")?),
	    libc::IFLA_PROTINFO	=> Self::ProtInfo(buf),
	    libc::IFLA_TXQLEN	=> Self::TxQLen(*Self::cast_buf(buf, "IFLA_TXQLEN")?),
	    libc::IFLA_OPERSTATE => Self::OperState(*Self::cast_buf(buf, "IFLA_OPERSTATE")?),
	    libc::IFLA_LINKMODE	=> Self::LinkMode(*Self::cast_buf(buf, "IFLA_LINKMODE")?),

	    _	=> Self::Raw(rta_type, buf),
	})
    }
}
