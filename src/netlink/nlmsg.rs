use crate::Result;

use super::ffi::nlmsghdr;

#[derive(Clone, Debug)]
pub struct NlMsgStream<'a> {
    buf:	&'a [u8],
    hdr:	nlmsghdr,
}

impl <'a> NlMsgStream<'a> {
    fn verify(buf: &'a [u8]) -> Result<()> {
	let mut buf = if buf.is_empty() {
	    None
	} else {
	    Some(buf)
	};

	while let Some(b) = buf {
	    let attr = nlmsghdr::from_slice(b)?;
	    buf = attr.next(b);
	}

	Ok(())
    }

    pub fn new(buf: &'a [u8]) -> Result<Self> {
	Self::verify(buf)?;

	Ok(Self::new_unchecked(buf))
    }

    fn new_unchecked(buf: &'a [u8]) -> Self {
	Self {
	    buf:	buf,
	    hdr:	*nlmsghdr::from_slice_unchecked(buf),
	}
    }

    pub fn get_payload(&self) -> &'a [u8] {
	self.hdr.payload(self.buf)
    }

    pub fn as_nlmsghdr(&self) -> &nlmsghdr {
	&self.hdr
    }

    pub fn next(&self) -> Option<Self> {
	self.hdr.next(self.buf).map(Self::new_unchecked)
    }

    pub fn iter(&self) -> NlMsgIterator<'a> {
	self.clone().into_iter()
    }
}

impl <'a> IntoIterator for NlMsgStream<'a> {
    type Item = NlMsgStream<'a>;
    type IntoIter = NlMsgIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
	NlMsgIterator {
	    attr:	Some(self)
	}
    }
}

pub struct NlMsgIterator<'a> {
    attr:	Option<NlMsgStream<'a>>,
}

impl <'a> Iterator for NlMsgIterator<'a> {
    type Item = NlMsgStream<'a>;

    fn next(&mut self) -> Option<Self::Item> {
	let attr = self.attr.take();

	match attr {
	    None	=> None,
	    Some(a)	=> {
		self.attr = a.next();
		Some(a)
	    }
	}
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_00() {
	#[repr(C)]
	struct Msg (u32, u16, u16, u32, u32,  [u8;23], u8,
		    u32, u16, u16, u32, u32,  [u8;24],
		    u32, u16, u16, u32, u32,
		    u32, u16, u16, u32, u32,  [u8;42]);

	let msg = Msg(16 + 23, 1, 0, 0, 0,  [0xff;23], 0xfe,
		      16 + 24, 2, 0, 0, 0,  [0xfd;24],
		      16 +  0, 3, 0, 0, 0,
		      16 + 42, 4, 0, 0, 0,  [0xfc;42]);

	let stream = NlMsgStream::new(unsafe {
	    std::slice::from_raw_parts(&msg as * const _ as * const u8, core::mem::size_of_val(&msg))
	}).unwrap();

	assert_eq!(stream.iter().count(), 4);

	for (idx, m) in stream.iter().enumerate() {
	    assert_eq!(m.as_nlmsghdr().nlmsg_type as usize, idx + 1);
	    assert_eq!(m.get_payload().len(), match idx {
		0	=> 23,
		1	=> 24,
		2	=>  0,
		3	=> 42,
		_	=> panic!()
	    });
	}
    }
}
