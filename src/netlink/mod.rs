mod fd;
pub mod ffi;
mod rt_attr;
mod rt_attr_ifla;
#[path = "nlmsg.rs"]
mod mod_nlmsg;

pub mod requests;

pub use fd::{ Fd, Request };

pub mod nlmsg {
    pub use super::mod_nlmsg::*;
}

pub mod rtattr {
    pub use super::rt_attr::{ RtAttrStream as Stream };
    pub use super::rt_attr_ifla::{ RtAttrIFLA as IFLA };
}


pub const NLMSG_DONE: u16 = nix::libc::NLMSG_DONE as u16;
pub const NLMSG_ERROR: u16 = nix::libc::NLMSG_ERROR as u16;
pub const RTM_NEWLINK: u16 = nix::libc::RTM_NEWLINK;
pub const RTM_DELLINK: u16 = nix::libc::RTM_DELLINK;
