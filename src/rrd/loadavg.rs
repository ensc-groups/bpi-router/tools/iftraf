use super::{ RrdDataSet, RrdDataSpec, RrdValue };
use crate::LoadAvg;

use super::DataType as DT;

const HEARTBEAT: u32 = 300;

static DS: &[RrdDataSpec] = &[
    RrdDataSpec {
	name:	"load_1",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Float(0.0),
	    max: RrdValue::Float(1_000_000.0),
	},
    },

    RrdDataSpec {
	name:	"proc_running",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Integer(0),
	    max: RrdValue::Integer(1_000_000),
	},
    },

    RrdDataSpec {
	name:	"proc_total",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Integer(0),
	    max: RrdValue::Integer(1_000_000),
	},
    },
];

// step: 2min
//
// 20160/2min  ... 28 days
// 24192/10min ...  6 months
// 16128/30min ...  1 year
// 13440/ 3h   ...  5 year
static RRA: &[&str] = &[
    "RRA:AVERAGE:0.5:1:20160",
    "RRA:AVERAGE:0.5:5:24192",
    "RRA:AVERAGE:0.5:15:16128",
    "RRA:AVERAGE:0.5:90:13448",

    "RRA:MAX:0.5:1:20160",
    "RRA:MAX:0.5:5:24192",
    "RRA:MAX:0.5:15:16128",
    "RRA:MAX:0.5:90:13448",

    "RRA:LAST:0.5:1:20160",
    "RRA:LAST:0.5:5:24192",
    "RRA:LAST:0.5:15:16128",
    "RRA:LAST:0.5:90:13448",
];

impl RrdDataSet for LoadAvg {
    fn get_base_name(&self) -> &str {
        "loadavg"
    }

    fn get_ds(&self)-> &[RrdDataSpec] {
        DS
    }

    fn get_rra(&self) -> &[&'static str] {
        RRA
    }

    fn get_values(&self) -> Vec<(crate::Time,Vec<RrdValue>)> {
	vec![(
	    self.tm, vec![
		self.load_1.into(),
		self.proc_r.into(),
		self.proc_total.into(),
	    ])]
    }

    fn get_min_tm(&self) -> crate::Time {
	self.tm - std::time::Duration::from_secs(60)
    }
}
