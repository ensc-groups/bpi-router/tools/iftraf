use nix::libc;

#[link(name = "rrd")]
extern "C" {
    pub fn rrd_updatex_r(
	filename: * const libc::c_char,
	template: * const libc::c_char,
	extra_flags: libc::c_int,
	argc: libc::c_int,
	argv: * const * const libc::c_char) -> libc::c_int;

    pub fn rrd_create_r(
	filename: * const libc::c_char,
	pdp_step:	libc::c_ulong,
	last_up:	libc::time_t,
	argc: libc::c_int,
	argv: * const * const libc::c_char) -> libc::c_int;

    pub fn rrd_get_error() -> * const libc::c_char;
    pub fn rrd_clear_error();
}
