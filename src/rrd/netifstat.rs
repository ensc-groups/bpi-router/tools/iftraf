use super::{ RrdDataSet, RrdDataSpec, RrdValue };
use crate::{ NetIfStat, NetIfStatSet };

use super::DataType as DT;

const HEARTBEAT: u32 = 300;
const MAX_DATARATE: i64 = 10_000_000_000;

static DS: &[RrdDataSpec] = &[
    RrdDataSpec {
	name:	"rx_packets",
        data_type: DT::Derive {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: MAX_DATARATE / 8 / 100
	},
    },

    RrdDataSpec {
	name:	"tx_packets",
        data_type: DT::Derive {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: MAX_DATARATE / 8 / 100,
	},
    },

    RrdDataSpec {
	name:	"rx_bytes",
        data_type: DT::Derive {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: MAX_DATARATE / 8,
	},
    },

    RrdDataSpec {
	name:	"tx_bytes",
        data_type: DT::Derive {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: MAX_DATARATE / 8,
	},
    },

    RrdDataSpec {
	name:	"rx_errors",
        data_type: DT::Derive {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: MAX_DATARATE / 1000,
	},
    },

    RrdDataSpec {
	name:	"tx_errors",
        data_type: DT::Derive {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: MAX_DATARATE / 1000,
	},
    },

    RrdDataSpec {
	name:	"rx_dropped",
        data_type: DT::Derive {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: MAX_DATARATE / 1000,
	},
    },

    RrdDataSpec {
	name:	"tx_dropped",
        data_type: DT::Derive {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: MAX_DATARATE / 1000,
	},
    },
];

// step: 2min
//
// 20160/2min  ... 28 days
// 24192/10min ...  6 months
// 16128/30min ...  1 year
// 13440/ 3h   ...  5 year
static RRA: &[&str] = &[
    "RRA:AVERAGE:0.5:1:20160",
    "RRA:AVERAGE:0.5:5:24192",
    "RRA:AVERAGE:0.5:15:16128",
    "RRA:AVERAGE:0.5:90:13448",

    "RRA:MAX:0.5:1:20160",
    "RRA:MAX:0.5:5:24192",
    "RRA:MAX:0.5:15:16128",
    "RRA:MAX:0.5:90:13448",

    "RRA:LAST:0.5:1:20160",
    "RRA:LAST:0.5:5:24192",
    "RRA:LAST:0.5:15:16128",
    "RRA:LAST:0.5:90:13448",
];

impl RrdDataSet for NetIfStat {
    fn get_ds(&self) -> &[super::RrdDataSpec] {
	DS
    }

    fn get_values(&self) -> Vec<(crate::Time, Vec<RrdValue>)> {
	vec![(
	    self.info.tm,
	    vec![
	    self.info.rx_packets.into(),
	    self.info.tx_packets.into(),
	    self.info.rx_bytes.into(),
	    self.info.tx_bytes.into(),
	    self.info.rx_errors.into(),
	    self.info.tx_errors.into(),
	    self.info.rx_dropped.into(),
	    self.info.tx_dropped.into(),
	    ]
	)]
    }

    fn get_rra(&self) -> &[&'static str] {
        RRA
    }

    fn get_min_tm(&self) -> crate::Time {
	self.info.tm - std::time::Duration::from_secs(60)
    }

    fn get_base_name(&self) -> &str {
        &self.ifname
    }
}

impl RrdDataSet for NetIfStatSet {
    fn get_ds(&self) -> &[super::RrdDataSpec] {
	DS
    }

    fn get_values(&self) -> Vec<(crate::Time, Vec<RrdValue>)> {
	let mut res = Vec::with_capacity(self.data.len());

	for d in &self.data {
	    res.push((d.tm, vec![
		d.rx_packets.into(),
		d.tx_packets.into(),
		d.rx_bytes.into(),
		d.tx_bytes.into(),
		d.rx_errors.into(),
		d.tx_errors.into(),
		d.rx_dropped.into(),
		d.tx_dropped.into(),
	    ]))
	}

	res
    }

    fn get_rra(&self) -> &[&'static str] {
        RRA
    }

    fn get_min_tm(&self) -> crate::Time {
	self.data.first().unwrap().tm - std::time::Duration::from_secs(60)
    }

    fn get_base_name(&self) -> &str {
        &self.ifname
    }
}
