use super::{ RrdDataSet, RrdDataSpec, RrdValue };
use crate::MemInfo;

use super::DataType as DT;

const HEARTBEAT: u32 = 300;
const MAX_MEM: i64 = 1_024 * 1_024 * 1_024 * 1_024; // 1 TiB

static DS: &[RrdDataSpec] = &[
    RrdDataSpec {
	name:	"mem_total",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Integer(0),
	    max: RrdValue::Integer(MAX_MEM),
	},
    },

    RrdDataSpec {
	name:	"mem_free",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Integer(0),
	    max: RrdValue::Integer(MAX_MEM),
	},
    },

    RrdDataSpec {
	name:	"mem_avail",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Integer(0),
	    max: RrdValue::Integer(MAX_MEM),
	},
    },

    RrdDataSpec {
	name:	"buffers",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Integer(0),
	    max: RrdValue::Integer(MAX_MEM),
	},
    },

    RrdDataSpec {
	name:	"cached",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Integer(0),
	    max: RrdValue::Integer(MAX_MEM),
	},
    },

    RrdDataSpec {
	name:	"swap",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Integer(0),
	    max: RrdValue::Integer(MAX_MEM),
	},
    },
];

// step: 2min
//
// 20160/2min  ... 28 days
// 24192/10min ...  6 months
// 16128/30min ...  1 year
// 13440/ 3h   ...  5 year
static RRA: &[&str] = &[
    "RRA:AVERAGE:0.5:1:20160",
    "RRA:AVERAGE:0.5:5:24192",
    "RRA:AVERAGE:0.5:15:16128",
    "RRA:AVERAGE:0.5:90:13448",

    "RRA:MAX:0.5:1:20160",
    "RRA:MAX:0.5:5:24192",
    "RRA:MAX:0.5:15:16128",
    "RRA:MAX:0.5:90:13448",

    "RRA:LAST:0.5:1:20160",
    "RRA:LAST:0.5:5:24192",
    "RRA:LAST:0.5:15:16128",
    "RRA:LAST:0.5:90:13448",
];

impl RrdDataSet for MemInfo {
    fn get_base_name(&self) -> &str {
        "meminfo"
    }

    fn get_ds(&self)-> &[RrdDataSpec] {
        DS
    }

    fn get_rra(&self) -> &[&'static str] {
        RRA
    }

    fn get_values(&self) -> Vec<(crate::Time,Vec<RrdValue>)> {
	vec![(
	    self.tm, vec![
		self.mem_total.into(),
		self.mem_free.into(),
		self.mem_avail.into(),
		self.buffers.into(),
		self.cached.into(),
		self.swap.into(),
	    ])]
    }

    fn get_min_tm(&self) -> crate::Time {
	self.tm - std::time::Duration::from_secs(60)
    }
}
