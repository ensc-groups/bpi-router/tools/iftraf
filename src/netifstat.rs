use std::collections::BTreeMap;

use nix::libc;

use crate::Time;

use crate::netlink;
use crate::netlink::{ rtattr, nlmsg };

use crate::{ Result, Error };

#[repr(C, align(16))]
struct RecvBuf<const N: usize>(
    [core::mem::MaybeUninit::<u8>; N]
);

impl <const N: usize> RecvBuf<N> {
    pub fn new() -> Self {
	Self([core::mem::MaybeUninit::uninit(); N])
    }
}

impl <const N: usize> std::ops::Deref for RecvBuf<N> {
    type Target = [core::mem::MaybeUninit::<u8>];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl <const N: usize> std::ops::DerefMut for RecvBuf<N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[derive(Debug)]
pub struct NetIfStatInfo {
    pub tm:			Time,
    pub prio:			i32,
    pub rx_packets:		u64,
    pub tx_packets:		u64,
    pub rx_bytes:		u64,
    pub tx_bytes:		u64,
    pub rx_errors:		u64,
    pub tx_errors:		u64,
    pub rx_dropped:		u64,
    pub tx_dropped:		u64,
}

#[derive(Debug)]
pub struct NetIfStat {
    pub ifname:			String,
    pub info:			NetIfStatInfo,
}

impl NetIfStat {
    fn parse_rtm_newlink(nl_payload: &[u8], now: Time) -> Result<Option<Self>>
    {
	let ifinfo = netlink::ffi::ifinfomsg::from_slice(nl_payload)?;
	let payload = ifinfo.payload(nl_payload);
	let rtattr = rtattr::Stream::new(payload)?;
	let mut stats = NetIfStatRaw::new(now);

	if (ifinfo.ifi_flags & (libc::IFF_UP as u32)) == 0 {
	    debug!("link #{} is down; skipping it", ifinfo.ifi_index);
	    return Ok(None);
	}

	for a in rtattr {
	    let a= a.try_into_ifla()?;

	    trace!("{:?}", a);
	    stats.register(a);
	}

	if !stats.is_valid() {
	    warn!("received stats are invalid");
	    return Err(Error::BadStats);
	}

	stats.cleanup();

	Ok(Some(stats.into()))
    }

    fn parse_nlmsg(all_stats: &mut Vec<Self>, resp: nlmsg::NlMsgStream, now: Time) -> Result<bool>
    {

	for nl in resp {
	    let hdr = &nl.as_nlmsghdr();

	    match hdr.nlmsg_type {
		netlink::NLMSG_DONE		=> {
		    debug!("stats = {:?}", all_stats);
		    return Ok(true);
		},

		netlink::NLMSG_ERROR		=> {
		    // todo: extract error code
		    warn!("nlmsg message failed");
		    return Ok(true);
		},

		netlink::RTM_NEWLINK |
		netlink::RTM_DELLINK => {
		    if let Some(mut stats) = Self::parse_rtm_newlink(nl.get_payload(), now)? {
			if hdr.nlmsg_type == netlink::RTM_DELLINK {
			    // DELLINK contains the most recent stats and there
			    // will not be generated further stats for this
			    // interface.  Increase priority so that it will be
			    // used in rrd_update when MIN_RRD_TIMESPAN has not
			    // been reached
			    stats.info.prio += 10;
			}
			all_stats.push(stats);
		    }
		},

		_			=>
		    warn!("unexpected nlmsg_type {}", hdr.nlmsg_type),
	    }
	}

	Ok(false)
    }

    pub fn request(fd: &crate::netlink::Fd) -> Result<()> {
	pub use netlink::requests::RTM_GETLINK;
	fd.send_request(RTM_GETLINK.0, RTM_GETLINK.1.as_bytes(), true)?;

	Ok(())
    }

    pub fn read(fd: &crate::netlink::Fd) -> Result<Vec<Self>> {
	let mut recv_buf: RecvBuf<{1024*1024}> = RecvBuf::new();
	let mut all_stats = Vec::new();

	loop {
	    let resp = fd.recv(&mut recv_buf, Some(crate::NLMSG_RX_TIMEOUT))?;
	    let now = Time::now();

	    if Self::parse_nlmsg(&mut all_stats, resp, now)? {
		break Ok(all_stats);
	    }
	}
    }
}

#[derive(Debug)]
struct NetIfStatRaw<'a> {
    tm:		Time,
    ifname:	Option<&'a std::ffi::CStr>,
    stat:	Option<&'a netlink::ffi::rtnl_link_stats>,
    stat64:	Option<&'a netlink::ffi::rtnl_link_stats64>,
}

impl <'a> NetIfStatRaw<'a> {
    pub fn new(tm: Time) -> Self {
	Self {
	    tm:		tm,
	    ifname:	None,
	    stat:	None,
	    stat64:	None,
	}
    }

    pub fn register(&mut self, info: rtattr::IFLA<'a>) {
	use rtattr::IFLA;

	match info {
	    IFLA::IfName(name)	=> self.ifname = Some(name),
	    IFLA::Stats(s)	=> self.stat = Some(s),
	    IFLA::Stats64(s)	=> self.stat64 = Some(s),
	    _			=> {},
	}
    }

    pub fn is_valid(&self) -> bool {
	self.ifname.is_some() && (
	    self.stat.is_some() || self.stat64.is_some()
	)
    }

    pub fn cleanup(&mut self) {
	if self.stat.is_some() && self.stat64.is_some() {
	    self.stat = None;
	}
    }
}

impl From<NetIfStatRaw<'_>> for NetIfStat {
    fn from(value: NetIfStatRaw) -> Self {
	match (value.stat, value.stat64) {
	    (_, Some(s))	=> {
		Self {
		    ifname:	value.ifname.unwrap().to_str().unwrap().to_owned(),
		    info:	NetIfStatInfo {
			tm:		value.tm,
			prio:		0,
			rx_packets:	s.rx_packets,
			tx_packets:	s.tx_packets,
			rx_bytes:	s.rx_bytes,
			tx_bytes:	s.tx_bytes,
			rx_errors:	s.rx_errors,
			tx_errors:	s.tx_errors,
			rx_dropped:	s.rx_dropped,
			tx_dropped:	s.tx_dropped,
		    }
		}
	    },
	    (Some(s), None)	=> {
		Self {
		    ifname:	value.ifname.unwrap().to_str().unwrap().to_owned(),
		    info:	NetIfStatInfo {
			tm:		value.tm,
			prio:		0,
			rx_packets:	s.rx_packets as u64,
			tx_packets:	s.tx_packets as u64,
			rx_bytes:	s.rx_bytes as u64,
			tx_bytes:	s.tx_bytes as u64,
			rx_errors:	s.rx_errors as u64,
			tx_errors:	s.tx_errors as u64,
			rx_dropped:	s.rx_dropped as u64,
			tx_dropped:	s.tx_dropped as u64,
		    }
		}
	    },

	    _			=>
		panic!("can not convert netif stats {value:?}"),
	}
    }
}

#[derive(Debug)]
pub struct NetIfStatSet {
    pub ifname:	String,
    pub data:	Vec<NetIfStatInfo>,
}

impl From<(String, Vec<NetIfStatInfo>)> for NetIfStatSet {
    fn from((ifname, data): (String, Vec<NetIfStatInfo>)) -> Self {
        Self { ifname, data }
    }
}

#[derive(Default)]
pub struct NetIfStatCollection(BTreeMap<String, Vec<NetIfStatInfo>>);

impl NetIfStatCollection {
    pub fn new() -> Self {
	Self(BTreeMap::new())
    }

    pub fn push(&mut self, ifname: String, info: NetIfStatInfo) {
	match self.0.get_mut(&ifname) {
	    None		=> {
		self.0.insert(ifname, vec![info]);
	    },

	    Some(data)	=> {
		let last_tm = data.last().as_ref().unwrap().tm;
		let last_prio = data.last().as_ref().unwrap().prio;

		match info.tm.duration_since(last_tm) {
		    Ok(d) if d >= crate::MIN_RRD_TIMESPAN	=> data.push(info),
		    Ok(_) if info.prio > last_prio		=> {
			*data.last_mut().unwrap() = info;
		    }
		    _						=> {
			warn!("stat {info:?} on {ifname} seen too early; ignoring it");
		    },
		}
	    },
	}
    }

    // TODO: enable when "map_first_last" is stable
    #[cfg(feature = "map_first_last")]
    pub fn pop(&mut self) -> Option<NetIfStatSet> {
	self.0.pop_first().map(NetIfStatSet::from)
    }

    pub fn pop(&mut self) -> Option<NetIfStatSet> {
	match self.iter().next() {
	    Some((ifname, _))	=> {
		let ifname = ifname.clone();
		Some(self.0.remove_entry(&ifname).unwrap().into())
	    },

	    None			=> None,
	}
    }
}

impl std::ops::Deref for NetIfStatCollection {
    type Target = BTreeMap<String, Vec<NetIfStatInfo>>;

    fn deref(&self) -> &Self::Target {
	&self.0
    }
}

impl std::ops::DerefMut for NetIfStatCollection {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
