#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    Nix(#[from] nix::Error),

    #[error("pending netlink request")]
    PendingRequest,

    #[error("bad nlmsg")]
    BadNlMsg,

    #[error("{} object misaligned", .0)]
    Misaligned(&'static str),

    #[error("{} object too large for buffer of {} bytes", .0, .1)]
    InsufficientBuffer(&'static str, usize),

    #[error("{} object too large for {} bytes", .0, .1)]
    TooSmall(&'static str, usize),

    #[error("{} object exceeds {} bytes", .0, .1)]
    TooLarge(&'static str, usize),

    #[error("netlink response misaligned")]
    NlResponseMisaligned,

    #[error("netlink response has bad size {}", .0)]
    NlResponseBadSize(usize),

    #[error("rtattr is misaligned")]
    RtAttrMisAligned,

    #[error("rtattr too short ({})", .0)]
    RtAttrTooShort(usize),

    #[error("bad cstring in {}", .0)]
    BadCString(&'static str),

    #[error("bad stats")]
    BadStats,

    #[error("rrd error {:?}", .0)]
    RrdError(std::ffi::CString),

    #[error("bad /proc/loadavg content: {0}")]
    BadLoadAvg(&'static str),

    #[error("bad /proc/meminfo content: {0}")]
    BadMemInfo(&'static str),
}

impl Error {
    pub fn from_errno<T>() -> Result<T, Self> {
	Err(Self::Io(std::io::Error::last_os_error()))
    }

    pub fn try_rc(rc: nix::libc::c_int) -> Result<(), Self> {
	if rc < 0 {
	    Self::from_errno()
	} else {
	    Ok(())
	}
    }
}
