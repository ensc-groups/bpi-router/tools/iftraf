use crate::{ Result, Error, Time };

#[derive(Debug)]
pub struct LoadAvg {
    pub tm:		Time,
    pub load_1:		f32,
    pub proc_r:		u32,
    pub proc_total:	u32,
}

impl LoadAvg {
    pub fn read(now: Time) -> Result<Self> {
	let content  = std::fs::read_to_string("/proc/loadavg")?;
	let mut info = content.split(' ');

	let load_1     = info.next().ok_or(Error::BadLoadAvg("not load_1 value"))?;
	let _load_5    = info.next().ok_or(Error::BadLoadAvg("not load_5 value"))?;
	let _load_15   = info.next().ok_or(Error::BadLoadAvg("not load_15 value"))?;
	let mut rstat  = info.next().ok_or(Error::BadLoadAvg("not rstat value"))?.split('/');
	let _pidmax    = info.next().ok_or(Error::BadLoadAvg("not pidmax value"))?;

	let proc_r     = rstat.next().ok_or(Error::BadLoadAvg("missing 'r' count in rstat"))?;
	let proc_t     = rstat.next().ok_or(Error::BadLoadAvg("missing 'r' count in rstat"))?;


	Ok(Self {
	    tm:		now,
	    load_1:	load_1.parse().map_err(|_| Error::BadLoadAvg("can not parse load_1 value"))?,
	    proc_r:	proc_r.parse().map_err(|_| Error::BadLoadAvg("can not parse proc_r value"))?,
	    proc_total:	proc_t.parse().map_err(|_| Error::BadLoadAvg("can not parse proc_total value"))?,
	})
    }
}
