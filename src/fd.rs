use std::os::unix::io::RawFd;

pub struct Fd(pub RawFd);

impl Fd {
    pub fn new(fd: RawFd) -> Self {
	Self(fd)
    }
}

impl std::os::unix::io::AsRawFd for Fd {
    fn as_raw_fd(&self) -> RawFd {
        self.0
    }
}

impl Drop for Fd {
    fn drop(&mut self) {
        unsafe { nix::libc::close(self.0) };
    }
}
