#![allow(clippy::redundant_field_names)]

use clap::Parser;

#[macro_use]
extern crate tracing;

mod ifacematch;
mod netlink;
mod errors;
mod fd;
mod netifstat;
mod loadavg;
mod meminfo;
mod rrd;

use ifacematch::IfaceMatch;
use nix::sys::{ timerfd, select, signalfd };

pub use netifstat::{ NetIfStat, NetIfStatCollection, NetIfStatSet };
pub use loadavg::LoadAvg;
pub use meminfo::MemInfo;
pub use errors::Error;
pub use fd::Fd;

pub type Result<T> = std::result::Result<T, Error>;

type Time = std::time::SystemTime;

// NOTE: when changing this value, the RRA setup in rrd/netifstat.rs must be also changed
const POLL_TIMER: std::time::Duration = std::time::Duration::from_secs(120);
const POLL_TIMERSPEC: nix::sys::time::TimeSpec = nix::sys::time::TimeSpec::from_duration(POLL_TIMER);

pub const NLMSG_RX_TIMEOUT: std::time::Duration = std::time::Duration::from_secs(5);
pub const MIN_RRD_TIMESPAN: std::time::Duration = {
    let a = std::time::Duration::from_secs(10);
    match POLL_TIMER.checked_div(10) {
	// TODO: rewrite when const traits have been implemented!
	Some(b)	=>	if a.as_nanos() < b.as_nanos() { a } else { b },
	None	=>	panic!("POLL_TIMER / 10 failed"),
    }
};

#[derive(clap::ValueEnum)]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum LogFormat {
    Default,
    Compact,
    Full,
    Json,
}

#[derive(clap::Parser, Debug)]
#[clap(author, version, about)]
struct CliOpts {
    #[clap(short, long, help("use systemd notification"), value_parser)]
    systemd:		bool,

    #[clap(short('L'), long, value_parser, value_name("FMT"), help("log format"),
	   default_value("default"))]
    log_format:		LogFormat,

    #[clap(short('R'), long, value_parser, help("directory for rrd files"))]
    rrddir:		std::path::PathBuf,

    #[clap(short('d'), long, value_parser, value_name("SECS"),
	   help("with this amount of seconds before fetching interface statistics"),
	   default_value("2"))]
    start_delay:	u32,

    #[clap(short('S'), long, value_parser, value_name("COUNT"),
	   help("only update the RRD data every COUNT of measurement; used to reduce disk activity"),
	   default_value("3"))]
    skip_rrd:		u32,

    #[clap(help("interfaces"), required(true), value_parser)]
    interfaces:		Vec<IfaceMatch>,
}

type NetIfStatVec = Vec<NetIfStat>;
type LoadAvgVec = std::collections::VecDeque<LoadAvg>;
type MemInfoVec = std::collections::VecDeque<MemInfo>;

fn read_stats(fd_nl: &netlink::Fd, stats: &mut NetIfStatVec, interfaces: &Vec<IfaceMatch>) {
    let cur_stats = match NetIfStat::read(fd_nl) {
	Err(e)	=> {
	    error!("failed to read device stats: {:?}", e);
	    return;
	},
	Ok(s)	=> s,
    };

    let cur_stats = cur_stats
	.into_iter()
	.filter(|s| IfaceMatch::matches_spec(interfaces, &s.ifname).is_yes());

    for s in cur_stats {
	stats.push(s);
    }
}

fn save_single_stat<T>(rrddir: &std::path::Path, prefix: &str, stat: T) -> Result<()>
where
    T: rrd::RrdDataSet + Sized
{
    let fname = prefix.to_owned() + stat.get_base_name() + ".rrd";
    let path = rrddir.join(fname);

    if !path.exists() {
	rrd::create(&path, stat.get_min_tm(), POLL_TIMER, &stat)?;
    }

    rrd::update(&path, &stat)
}

fn save_stats(rrddir: &std::path::Path, stats: &mut NetIfStatCollection) {
    while let Some(s) = stats.pop() {
	debug!("rrd -> {:?}", s);

	if let Err(e) = save_single_stat(rrddir, "netif:", s) {
	    error!("failed to save stat: {:?}", e);
	}
    }
}

fn save_loadavg(rrddir: &std::path::Path, stats: &mut LoadAvgVec) {
    while let Some(s) = stats.pop_front() {
	debug!("rrd -> {:?}", s);

	if let Err(e) = save_single_stat(rrddir, "", s) {
	    error!("failed to save stat: {:?}", e);
	}
    }
}

fn save_meminfo(rrddir: &std::path::Path, stats: &mut MemInfoVec) {
    while let Some(s) = stats.pop_front() {
	debug!("rrd -> {:?}", s);

	if let Err(e) = save_single_stat(rrddir, "", s) {
	    error!("failed to save stat: {:?}", e);
	}
    }
}

fn do_notify(state: sd_notify::NotifyState, use_systemd: bool) {
    if use_systemd {
	let _ = sd_notify::notify(false, &[state]);
    }
}

fn main() -> Result<()> {
    use nix::sys::signal::Signal;

    let mut args = CliOpts::parse();

    if args.log_format == LogFormat::Default {
	args.log_format = if args.systemd {
	    // when running under systemd, do not emit the timestamp because
	    // output is usually recorded in the journal.  Accuracy in journal
	    // should suffice for most usecases.

	    LogFormat::Compact
	} else {
	    LogFormat::Full
	}
    }

    let fmt = tracing_subscriber::fmt()
	.with_env_filter(tracing_subscriber::EnvFilter::from_default_env());

    match args.log_format {
	LogFormat::Compact		=> fmt.without_time().init(),
	LogFormat::Json			=> fmt.json().init(),
	LogFormat::Full			=> fmt.init(),
	LogFormat::Default		=> unreachable!(),
    }

    let sigmask = {
	let mut mask = signalfd::SigSet::empty();
	mask.add(Signal::SIGINT);
	mask.add(Signal::SIGUSR1);
	mask.add(Signal::SIGTERM);
	mask
    };

    let interfaces = args.interfaces.clone();
    let rrddir = args.rrddir;

    let fd_nl = netlink::Fd::new()?;
    let fd_tm = timerfd::TimerFd::new(timerfd::ClockId::CLOCK_BOOTTIME,
				      timerfd::TimerFlags::TFD_CLOEXEC |
				      timerfd::TimerFlags::TFD_NONBLOCK)?;
    let mut fd_sig = signalfd::SignalFd::with_flags(&sigmask,
						signalfd::SfdFlags::SFD_CLOEXEC |
						signalfd::SfdFlags::SFD_NONBLOCK)?;

    let mut stats: NetIfStatCollection = Default::default();
    let mut loadavgs = LoadAvgVec::new();
    let mut meminfos = MemInfoVec::new();

    do_notify(sd_notify::NotifyState::Ready, args.systemd);

    if args.start_delay > 0 {
	// prevent rrd errors due to the same time on subsequent, fast restarts
	// by delaying startup
	do_notify(sd_notify::NotifyState::WatchdogUsec(args.start_delay * 1_000_000 * 2),
						       args.systemd);

	std::thread::sleep(std::time::Duration::from_secs(args.start_delay.into()));
    }

    NetIfStat::request(&fd_nl)?;
    fd_tm.set(timerfd::Expiration::IntervalDelayed(POLL_TIMERSPEC, POLL_TIMERSPEC),
	      timerfd::TimerSetTimeFlags::empty())?;

    do_notify(sd_notify::NotifyState::WatchdogUsec(POLL_TIMER.as_micros() as u32 * 2), args.systemd);
    do_notify(sd_notify::NotifyState::Watchdog, args.systemd);

    sigmask.thread_block()?;

    info!("started");

    let mut is_alive = true;
    let mut skip_rrd = None;
    let mut pending_request = true;

    while is_alive {
	use std::os::unix::io::AsRawFd;

	let mut fdset = select::FdSet::new();
	let mut do_nl = false;
	let mut do_sysstat = skip_rrd.is_none(); // None marks first loop

	fdset.insert(fd_nl.as_raw_fd());
	fdset.insert(fd_tm.as_raw_fd());
	fdset.insert(fd_sig.as_raw_fd());

	match select::select(None, &mut fdset, None, None, None) {
	    Err(nix::Error::EINTR)	=> continue,
	    v				=> v,
	}?;

	if fdset.contains(fd_sig.as_raw_fd()) {
	    match fd_sig.read_signal() {
		Ok(Some(info)) if [Signal::SIGTERM as u32,
				   Signal::SIGKILL as u32,
				   Signal::SIGINT as u32].contains(&info.ssi_signo)	=> {
		    debug!("got termination signal; quitting");
		    is_alive = false;
		    do_nl = true;
		    do_sysstat = true;
		    skip_rrd = Some(0);
		    do_notify(sd_notify::NotifyState::Stopping, args.systemd);
		},

		Ok(Some(info)) if [Signal::SIGUSR1 as u32].contains(&info.ssi_signo)	=> {
		    info!("got SIGUSR1; dumping iface stats");
		    do_nl = true;
		    do_sysstat = true;
		    skip_rrd = Some(0);
		    do_notify(sd_notify::NotifyState::Status("got SIGUSR1; dumping iface stats"),
			      args.systemd);
		}

		_				=> {},
	    }
	}

	if fdset.contains(fd_tm.as_raw_fd()) {
	    let _ = fd_tm.wait();
	    do_nl = true;
	    do_sysstat = true;

	    do_notify(sd_notify::NotifyState::Watchdog, args.systemd);
	}

	if do_nl && !pending_request {
	    match NetIfStat::request(&fd_nl) {
		Err(e)	=> error!("failed to send nlmsg: {:?}", e),
		Ok(_)	=> pending_request = true,
	    }
	}

	if fdset.contains(fd_nl.as_raw_fd()) {
	    let mut cur_stats = NetIfStatVec::new();

	    read_stats(&fd_nl, &mut cur_stats, &interfaces);

	    pending_request = false;

	    for s in cur_stats {
		stats.push(s.ifname, s.info);
	    }

	    match skip_rrd {
		Some(v) if v > 0	=> skip_rrd = Some(v - 1),
		_			=> { },
	    }
	}

	if do_sysstat {
	    match LoadAvg::read(Time::now()) {
		Err(e)	=> error!("failed to read loadavg: {:?}", e),
		Ok(avg)	=> loadavgs.push_back(avg),
	    }

	    match MemInfo::read(Time::now()) {
		Err(e)	=> error!("failed to read meminfo: {:?}", e),
		Ok(mem)	=> meminfos.push_back(mem),
	    }
	}

	match skip_rrd {
	    None | Some(0)	=> {
		save_stats(&rrddir, &mut stats);
		save_loadavg(&rrddir, &mut loadavgs);
		save_meminfo(&rrddir, &mut meminfos);

		skip_rrd = Some(args.skip_rrd);
	    },
	    _		=>	debug!("skipping rrd update"),
	}
    }

    Ok(())
}
